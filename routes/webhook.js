const express = require('express');
const router  = express.Router();
const line    = require('@line/bot-sdk');
const config  = require('../config/config');
const client  = new line.Client(config)

const { chatbotResponse } = require('../util/chatbot');
const defaultMessage      = require('../data/default.json');

router.route("/")
  .post(line.middleware(config), (req, res) => {
    Promise
      .all(req.body.events.map(handleEvent))
      .then((result) => res.json(result))
      .catch((err) => {
        console.error(err);
        res.status(500).end();
      });
  })

function handleEvent(event) {
  if (event.replyToken && event.replyToken.match(/^(.)\1*$/)) {
    return console.log("Test hook recieved: " + JSON.stringify(event.message));
  }
  
  if (event.type !== 'message' || event.message.type !== 'text') {
    // ignore non-text-message event
    return Promise.resolve(null);
  }

  var response = defaultMessage.error;

  try {
    const message = event.message.text;
    response = chatbotResponse(message);
  } catch(error) {
    console.log(error);
  }

  const echo = { type: 'text', text: response };
  return client.replyMessage(event.replyToken, echo)
}

module.exports = router;