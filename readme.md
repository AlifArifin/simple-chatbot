
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Conversational Agent</h3>

  <p align="center">
    Prosa.ai Problem 1
  <p>
</p>

<!-- ABOUT THE PROJECT -->
## About The Project

Simple chatbot application implemented as a web application through websocket. User question will be answered using the answer of a similar question in QA pairs.


### Built With

* [NodeJS](https://nodejs.org/en/)
* [Express](https://expressjs.com/)
* [Socket.io](https://socket.io/)
* [Docker](https://docker.com/)
* [Line Bot](https://developers.line.biz/en/)


### Project Idea
The idea behind this chatbot is, everytime chatbot receive a request, it will compare with its 'database' (pairs of Q and A) and find the best match that fulfills the threshold. List of the pairs can be accessed in `data/corpus.json`.

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* yarn; access this url [Yarn Installation](https://classic.yarnpkg.com/en/docs/install) for detail information
* docker; access this url [Docker Installation](https://docs.docker.com/get-docker/) for detail information

### Installation
 
1. Clone the repo
```sh
git clone https://gitlab.com/AlifArifin/simple-chatbot.git
```

2. Install Yarn packages; if you want to run in docker, you do not have to do this step
```sh
yarn install
```

### Run
#### Local environment
1. Start the project
```sh
node index.js
```
2. You can access this project in `localhost:3000`. If you change the port, just adjust it.
#### Using Docker
1. Build image
```sh
docker build -t <your username>/<project name> .
```
2. Run the image; if you want to use another port, just change it.
```sh
docker run -p 3000:3000 -d <your username>/<project name>
```
3. You can access this project in `localhost:3000`. If you change the port, just adjust it.

## Demo
This project is already deployed in `https://salty-falls-49103.herokuapp.com/`, feel free to use it. And also, this chatbot is deployed in Line, you can add it by this id `@701wtvos` or by this QR Code

![Bot QR Code](/static/img/bot.png)