require('dotenv').config({path: __dirname + '/.env'});

const express = require('express');
const app     = express();
const http    = require('http').Server(app);
const io      = require('socket.io')(http);
const port    = process.env.PORT || 3000

const defaultMessage      = require('./data/default.json');
const { chatbotResponse } = require('./util/chatbot')

app.use(express.static(__dirname + '/static'));
app.use(require('./routes'));

app.get('/', function(_, res) {
  res.sendFile('static/html/index.html', { root: __dirname });
});

// called when user connect
io.on('connection', function(socket) {
  console.log('A user connected:', socket.id);

  // greeting message
  io.in(socket.id).emit('user_message', defaultMessage.greeting);

  socket.on('user_message', (message) => {
    console.log('message: ' + message);

    var response = defaultMessage.error;
    try {
      response = chatbotResponse(message);
    } catch(error) {
      console.log(error);
    }
    socket.emit('user_message', response);
  });

  // called when user disconnect
  socket.on('disconnect', function () {
     console.log('A user disconnected');
  });
})

http.listen(port, function() {
  console.log('listening on port', port);
});