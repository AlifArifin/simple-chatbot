const channelAccessToken = process.env.CHANNEL_ACCESS_TOKEN;
const channelSecret      = process.env.CHANNEL_SECRET;

module.exports = {
  channelAccessToken : channelAccessToken,
  channelSecret      : channelSecret
};