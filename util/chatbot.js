const stringSimilarity  = require('string-similarity');
const corpus            = require('../data/corpus.json')
const threshold         = process.env.THRESHOLD || 0.5

function chatbotResponse(userMessage) {
  const messages = corpus.map(item => item['message']);
  const result = stringSimilarity.findBestMatch(userMessage, messages);

  if (result.bestMatch.rating < threshold) {
    throw('Cannot find the best match');
  }

  const bestResponse = corpus[result.bestMatchIndex].response;
  
  return bestResponse;
}

module.exports = {
  chatbotResponse
}